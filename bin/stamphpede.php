#!/usr/bin/env php
<?php

define('CONFIG_PATH_STACK', ['/stamphpede', getcwd(), '/../' . __DIR__,]);

require __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;
use Stamphpede\Config\ConfigLoader;
use Stamphpede\ServiceManager\ContainerBuilder;
use Symfony\Component\Console\Application;
use Stamphpede\Server\Console\ServeCommand;

// Load Env File
$dotenv = Dotenv::createImmutable(CONFIG_PATH_STACK, '.env');
$dotenv->load();

//@TODO question: should we load the .env from the working directory instead of the stamphede directory to improve developer workflow?

use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;

$containerBuilder = new ContainerBuilder([
    \Stamphpede\Config\Module::class,
    \Stamphpede\Parser\Module::class,
    \Stamphpede\Server\Module::class,
]);

$container = $containerBuilder->buildContainer();

$configLoader = $container->get(ConfigLoader::class);
$container->add('config', $configLoader->loadConfig(CONFIG_PATH_STACK, 'config.yaml'));

$commandLoader = new ContainerCommandLoader($container, [
    'serve' => ServeCommand::class,
]);

$application = new Application();
$application->setCommandLoader($commandLoader);

$application->run();
