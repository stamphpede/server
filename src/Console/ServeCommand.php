<?php

namespace Stamphpede\Server\Console;

use Psr\Log\LoggerInterface;
use React\EventLoop\LoopInterface;
use React\Http\Server;
use Stamphpede\Server\Service\LogMultiplexer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

class ServeCommand extends Command
{
    protected static $defaultName = 'serve';
    private LoggerInterface $logger;
    private LoopInterface $loop;
    private Server $server;

    public function __construct(Server $server, LoggerInterface $logger, LoopInterface $loop)
    {
        parent::__construct(ServeCommand::$defaultName);

        $this->logger = $logger;
        $this->loop = $loop;
        $this->server = $server;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Runs Stamphpede API server')
            ->addArgument('port', InputArgument::OPTIONAL, 'Port to run server on', 9000);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $port = $input->getArgument('port');

        if ($this->logger instanceof LogMultiplexer) {
            $logger = new ConsoleLogger($output);
            $this->logger->attach($logger);
        }

        $socket = new \React\Socket\Server('0.0.0.0:' . $port, $this->loop);
        $this->server->listen($socket);

        $output->writeln('Listening on ' . str_replace('tcp:', 'http:', $socket->getAddress()));

        $this->loop->run();

        return 0;
    }
}
