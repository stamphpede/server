<?php

namespace Stamphpede\Server\Entity;

use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as JMS;
use Stamphpede\Server\Runner\RunnerStats;

/**
 * @Hateoas\Relation("self", href = "expr('/run/' ~ object.getId())")
 * @Hateoas\Relation("results", href = "expr('/run/' ~ object.getId() ~ '/results')")
 */
class Run implements EntityInterface
{
    private const STATUS_RUNNING = 'running';
    private const STATUS_FINISHED = 'finished';

    private string $id;
    private string $testSuite;
    private string $status = self::STATUS_RUNNING;
    /** @JMS\Exclude() */
    private ?RunnerStats $stats = null;

    public function __construct(string $id, string $testSuite)
    {
        $this->id = $id;
        $this->testSuite = $testSuite;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function finished(RunnerStats $stats): void
    {
        $this->status = self::STATUS_FINISHED;
        $this->stats = $stats;
    }

    public function getStats(): ?RunnerStats
    {
        return $this->stats;
    }
}
