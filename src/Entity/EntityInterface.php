<?php

namespace Stamphpede\Server\Entity;

interface EntityInterface
{
    public function getId(): string;
}
