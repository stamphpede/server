<?php

namespace Stamphpede\Server\Entity;

use Hateoas\Configuration\Annotation as Hateoas;
use Stamphpede\Parser\Stamphpede;
use JMS\Serializer\Annotation as Jms;

/**
 * @Hateoas\Relation("self", href = "expr('/test-suite/' ~ object.getId())")
 */
class TestSuite implements EntityInterface
{
    private string $id;
    private ?int $duration = null;
    private ?int $rampUp = null;
    private ?int $concurrency = null;
    /** @JMS\Exclude() */
    private ?Stamphpede $stamphpede = null;
    private string $status = 'parsing';

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function getRampUp(): ?int
    {
        return $this->rampUp;
    }

    public function getConcurrency(): ?int
    {
        return $this->concurrency;
    }

    public function getStamphpede(): ?Stamphpede
    {
        return $this->stamphpede;
    }

    public function stamphpede(Stamphpede $stamphpede)
    {
        $this->duration = $stamphpede->getDuration();
        $this->rampUp = $stamphpede->getRampUp();
        $this->concurrency = $stamphpede->getConcurrency();
        $this->stamphpede = $stamphpede;
        $this->status = 'ready';
    }
}
