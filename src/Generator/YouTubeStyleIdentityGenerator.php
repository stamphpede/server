<?php

namespace Stamphpede\Server\Generator;

/**
 * Generates a youtube style id matching regex: [0-9A-Za-z-_]{11}
 */
class YouTubeStyleIdentityGenerator implements Generator
{
    public function generate(): string
    {
        return rtrim(strtr(base64_encode(random_bytes(22)), '+/', '-_'), '=');
    }
}
