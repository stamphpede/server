<?php

namespace Stamphpede\Server\Generator;

interface Generator
{
    public function generate(): string;
}
