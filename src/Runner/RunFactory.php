<?php

namespace Stamphpede\Server\Runner;

use Psr\Log\LoggerInterface;
use React\EventLoop\LoopInterface;
use React\Promise\Deferred;
use React\Promise\Promise;
use Stamphpede\Server\Entity\TestSuite;

class RunFactory
{
    private Browser $browser;
    private LoopInterface $loop;
    private LoggerInterface $logger;

    public function __construct(
        LoopInterface $loop,
        LoggerInterface $logger,
        Browser $browser
    ) {
        $this->browser = $browser;
        $this->loop = $loop;
        $this->logger = $logger;
    }

    public function createRun(TestSuite $testSuite): Promise
    {
        $stamphpede = $testSuite->getStamphpede();
        $runnerStats = new RunnerStats();

        $herd = new Herd($stamphpede->getTasks());
        for ($i = 0; $i < $stamphpede->getStartingConcurrency(); $i++) {
            $herd->newElephpant();
        }

        $run = new Run($this->browser, $this->loop, $herd, $stamphpede, $runnerStats);

        if ($stamphpede->getRampUp() > 0) {
            $timer = $this->loop->addPeriodicTimer(
                $stamphpede->getRampUpStepTime(),
                function () use ($stamphpede, $herd) {
                    for ($i = 0; $i < $stamphpede->getRampUpStep(); $i++) {
                        $herd->newElephpant();
                    }
                }
            );

            $this->loop->addTimer($stamphpede->getRampUp(), function () use ($timer) {
                $this->loop->cancelTimer($timer);
            });
        }

        $promise = new Deferred();

        $this->loop->addTimer($stamphpede->getDuration(), function () use ($runnerStats, $promise, $run, $herd) {
            $run->stop();

            $timer = null;
            $timer = $this->loop->addPeriodicTimer(1, function () use ($runnerStats, $herd, &$timer, $promise) {
                if ($herd->isFinished()) {
                    $promise->resolve($runnerStats);
                    $this->loop->cancelTimer($timer);
                }
            });
        });

        $this->logger->debug('---------------------------------------------------------');
        $this->logger->debug("Initial Concurrency: " . $stamphpede->getStartingConcurrency());
        $this->logger->debug("Concurrency: " . $stamphpede->getConcurrency());
        $this->logger->debug("Ramp Up: " . $stamphpede->getRampUp());
        $this->logger->debug("Duration: " . $stamphpede->getDuration());
        $this->logger->debug('---------------------------------------------------------');

        $this->loop->futureTick($run);

        return $promise->promise();
    }
}
