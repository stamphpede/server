<?php

namespace Stamphpede\Server\Runner;

use Stamphpede\Request;
use Stamphpede\Response;

class RunnerStats
{
    private array $data;

    public function __construct()
    {
    }

    public function addRequest(Request $request): void
    {
        $taskName = $request->getTaskName();

        if (!isset($this->data[$taskName])) {
            $this->data[$taskName] = [
                'method' => strtoupper($request->getMethod()),
                'url' => $request->getUrl(),
                'numRequests' => 0
            ];
        }

        $this->data[$taskName]['numRequests']++;
    }

    public function addResponse(Response $response, float $responseTime): void
    {
        $taskName = $response->getName();

        if (!isset($this->data[$taskName])) {
            throw new \InvalidArgumentException('No request entry key found for task: ' . $response->getName());
        }

        if (!isset($this->data[$taskName]['responses'])) {
            $this->data[$taskName]['responses']['times'] = [];
            $this->data[$taskName]['responses']['numFailures'] = 0;
        }

        $this->data[$taskName]['responses']['times'][] = $responseTime;

        if ($response->hasFailed()) {
            $this->data[$taskName]['responses']['numFailures']++;
        }
    }

    public function getAverageResponseTime(array $responseTimes): int
    {
        return $this->convertMicrotimeToMilliseconds(
            array_sum($responseTimes) / count($responseTimes)
        );
    }

    public function getMinResponseTimeInMilliseconds(array $responseTimes): int
    {
        return $this->convertMicrotimeToMilliseconds(
            min($responseTimes)
        );
    }

    public function getMaxResponseTimeInMilliseconds(array $responseTimes): int
    {
        return $this->convertMicrotimeToMilliseconds(
            max($responseTimes)
        );
    }

    private function convertMicrotimeToMilliseconds(float $time): int
    {
        return (int) round($time * 1000);
    }

    public function getData(): array
    {
        return $this->data;
    }
}
