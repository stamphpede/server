<?php

namespace Stamphpede\Server\Runner;

use Stamphpede\Response;
use Stamphpede\Server\Runner\JourneyEnded;
use Stamphpede\Server\Runner\Elephpant;
use Stamphpede\Request;

class Journey
{
    private const STATE_IDLE = 'idle';
    private const STATE_RUNNING = 'running';
    private const STATE_FINISHED = 'finished';

    private int $journeyNumber = 0;
    private string $state = self::STATE_IDLE;
    private Elephpant $elephpant;

    public function __construct(int $journey, Elephpant $elephpant)
    {
        $this->journeyNumber = $journey;
        $this->elephpant = $elephpant;
    }

    public function getJourneyNumber(): int
    {
        return $this->journeyNumber;
    }

    public function isFinished(): bool
    {
        return $this->state === self::STATE_FINISHED;
    }

    public function isIdle(): bool
    {
        return $this->state === self::STATE_IDLE;
    }

    public function idle(): void
    {
        $this->state = self::STATE_IDLE;
    }

    public function running(): void
    {
        $this->state = self::STATE_RUNNING;
    }

    public function nextRequest(): ?Request
    {
        try {
            $request = $this->elephpant->nextRequest();
        } catch (JourneyEnded $e) {
            $this->state = self::STATE_FINISHED;
            throw $e;
        }

        return $request;
    }

    public function responseReceived(Request $request, Response $response): void
    {
        $this->state = self::STATE_IDLE;
        $this->elephpant->responseReceived($request, $response);
    }

    public function isRunning()
    {
        return $this->state === self::STATE_RUNNING;
    }

    public function responseTimedOut()
    {
        $this->state = self::STATE_IDLE;
    }
}
