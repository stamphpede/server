<?php

namespace Stamphpede\Server\Runner;

use Stamphpede\Parser\TaskCollection;

class Herd extends \FilterIterator
{
    private \ArrayIterator $elephpantJourneys;
    private int $journeyCount = 0;
    private TaskCollection $tasks;

    public function __construct(TaskCollection $tasks)
    {
        $this->elephpantJourneys = new \ArrayIterator();
        parent::__construct($this->elephpantJourneys);
        $this->tasks = $tasks;
    }

    public function accept(): bool
    {
        return parent::current()->isIdle();
    }

    public function newElephpant(): void
    {
        $journey = new Journey($this->journeyCount, new Elephpant($this->tasks));
        $this->elephpantJourneys->append($journey);
        $this->journeyCount++;
    }

    public function isFinished(): bool
    {
        foreach ($this->elephpantJourneys->getArrayCopy() as $journey) {
            /** @var Journey $journey */
            if ($journey->isRunning()) {
                return false;
            }
        }

        return true;
    }
}
