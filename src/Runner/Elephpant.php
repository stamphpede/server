<?php

namespace Stamphpede\Server\Runner;

use Stamphpede\Server\Runner\JourneyEnded;
use Stamphpede\Server\Runner\UnresolvableDependencies;
use Stamphpede\Request;
use Stamphpede\ResponseCollection;
use Stamphpede\Parser\Task;
use Stamphpede\Parser\TaskCollection;

class Elephpant
{
    private ResponseCollection $previousRequests;
    private TaskCollection $tasks;
    private int $totalWeight = 0;

    public function __construct(TaskCollection $tasks)
    {
        $this->tasks = $tasks;

        foreach ($tasks as $task) {
            $this->totalWeight += $task->getWeight();
        }

        $this->previousRequests = new ResponseCollection();
    }

    public function responseReceived($request, $response)
    {
        $this->previousRequests[$request->getTaskName()] = $response;
    }

    public function nextRequest(): ?Request
    {
        do {
            $task = $this->pickTask();

            if (!isset($this->previousRequests[$task->getName()])) {
                break;
            }
            switch (true) {
                case $task->ignoreRepeat():
                    $task = null;
                    break;
                case $task->skipRepeat():
                    return null;
                case $task->endRepeat():
                    throw new JourneyEnded('A none repeatable task was picked again, which was set to end the journey when that happened');
            }
        } while ($task === null);

        $impossibleTasks = [];

        do {
            $dependenciesMet = true;
            $tryNext = null;
            $responsesForTask = new ResponseCollection();
            foreach ($task->getDepends() as $depend) {
                if (!isset($this->previousRequests[$depend])) {
                    $dependenciesMet = false;
                    if (!in_array($depend, $impossibleTasks)) {
                        $tryNext = $depend;
                    }
                } else {
                    $responsesForTask[$depend] = $this->previousRequests[$depend];
                }
            }

            if ($dependenciesMet) {
                break;
            }

            $impossibleTasks[] = $task->getName();

            if ($tryNext === null) {
                throw new UnresolvableDependencies(sprintf('Task %s has dependencies which cannot be resolved', $task->getName()));
            }

            $task = $this->tasks->get($tryNext);

        } while (true);

        return $task->getRequest($responsesForTask);
    }

    private function pickTask(): Task
    {
        $pick = rand(1, $this->totalWeight);

        foreach ($this->tasks as $task) {
            if ($pick <= $task->getWeight()) {
                return $task;
            }

            $pick -= $task->getWeight();
        }

        return $task;
    }
}
