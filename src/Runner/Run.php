<?php

namespace Stamphpede\Server\Runner;

use React\EventLoop\LoopInterface;
use Stamphpede\Server\Runner\JourneyEnded;
use Stamphpede\Parser\Stamphpede;

class Run
{
    private Browser $browser;
    private Herd $herd;
    private Stamphpede $stamphpede;
    private LoopInterface $loop;
    private bool $running = true;
    private RunnerStats $runnerStats;

    public function __construct(Browser $browser, LoopInterface $loop, Herd $herd, Stamphpede $stamphpede, RunnerStats $runnerStats)
    {
        $this->browser = $browser;
        $this->herd = $herd;
        $this->stamphpede = $stamphpede;
        $this->loop = $loop;
        $this->runnerStats = $runnerStats;
    }

    public function stop(): void
    {
        $this->running = false;
    }

    public function __invoke()
    {
        foreach ($this->herd as $journey) {

            if (!$journey->isIdle()) {
                continue;
            }

            try {
                $request = $journey->nextRequest();
            } catch (JourneyEnded $e) {
                $this->herd->newElephpant();
                continue;
            }

            // If we don't have a valid request, skip
            if ($request === null) {
                continue;
            }

            $this->browser->send($request, $journey, $this->runnerStats);
        }

        if ($this->running) {
            $this->loop->futureTick($this);
        }
    }
}
