<?php

namespace Stamphpede\Server\Runner;

use Clue\React\Buzz\Message\ResponseException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Clue\React\Buzz\Browser as ReactBrowser;
use Stamphpede\Request;
use Stamphpede\Response;

class Browser
{
    private LoggerInterface $logger;
    private ReactBrowser $browser;

    public function __construct(ReactBrowser $browser, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->browser = $browser->withTimeout(10.0);
    }

    public function send(Request $request, Journey $journey, RunnerStats $runnerStats): void
    {
        $psrRequest = $request->getPsrRequest();

        $journey->running();

        $this->logger->debug('REQUEST: [Journey: ' . $journey->getJourneyNumber() . '][Task: ' . $request->getTaskName() . '] Connecting to: ' . $psrRequest->getUri());
        $runnerStats->addRequest($request);

        $startTime = microtime(true);

        $this->browser->send($psrRequest)->then(
            function (ResponseInterface $psrResponse) use ($journey, $request, $startTime, $runnerStats) {
                // @todo - there's a bug which results in this value: float(-0.025669097900391). Unsure how a response time could be before a start time.
                $responseTime = microtime(true) - $startTime;

                $this->logger->debug('RESPONSE: [Journey: ' . $journey->getJourneyNumber() . '][Task: ' . $request->getTaskName() . '] ' . $psrResponse->getBody());

                $response = new Response($request->getTaskName(), $psrResponse);

                $journey->responseReceived($request, $response);
                $runnerStats->addResponse($response, $responseTime);
            },
            function (\Exception $exception) use ($journey, $request, $startTime, $runnerStats) {
                if ($exception instanceof ResponseException) {
                    $psrResponse = $exception->getResponse();

                    // @todo - there's a bug which results in this value: float(-0.025669097900391). Unsure how a response time could be before a start time.
                    $responseTime = microtime(true) - $startTime;

                    $this->logger->debug('RESPONSE: [Journey: ' . $journey->getJourneyNumber() . '][Task: ' . $request->getTaskName() . '] ' . $psrResponse->getBody());

                    $response = new Response($request->getTaskName(), $psrResponse);

                    $journey->responseReceived($request, $response);
                    $runnerStats->addResponse($response, $responseTime);
                } else {
                    $this->logger->debug('RESPONSE: [Journey: ' . $journey->getJourneyNumber() . '][Task: ' . $request->getTaskName() . '] ' . $exception->getMessage());
                    $journey->responseTimedOut();
                }
            }
        );
    }
}
