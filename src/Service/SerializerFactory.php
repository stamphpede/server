<?php

namespace Stamphpede\Server\Service;

use Hateoas\HateoasBuilder;
use JMS\Serializer\SerializerInterface;

class SerializerFactory
{
    public static function create(): SerializerInterface
    {
        return HateoasBuilder::create()->build();
    }
}
