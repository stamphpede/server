<?php

namespace Stamphpede\Server\Service;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

class LogMultiplexer implements LoggerInterface
{
    use LoggerTrait;

    private $loggers;

    public function __construct()
    {
        $this->loggers = new \SplObjectStorage();
    }

    public function log($level, $message, array $context = array())
    {
        foreach ($this->loggers as $logger) {
            $logger->log($level, $message, $context);
        }
    }

    public function attach(LoggerInterface $logger): void
    {
        $this->loggers->attach($logger);
    }

    public function detach(LoggerInterface $logger): void
    {
        $this->loggers->detach($logger);
    }
}
