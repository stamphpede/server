<?php

namespace Stamphpede\Server\Service;

use FastRoute\DataGenerator\GroupCountBased;
use FastRoute\RouteCollector;
use FastRoute\RouteParser\Std;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Stamphpede\Server\Router;
use Stamphpede\Server\Handler\TestSuite as TestSuiteHandlers;
use Stamphpede\Server\Handler\Run as RunHandlers;
use Stamphpede\Server\Handler\Result as ResultHandlers;
use Stamphpede\Server\Handler\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

class RouterFactory
{
    private static $routes = [
        TestSuiteHandlers\Create::class => [
            'method' => 'POST',
            'route' => '/test-suite',
            'factory' => 'testSuite_Create',
            'security' => true,
        ],
        TestSuiteHandlers\One::class => [
            'method' => 'GET',
            'route' => '/test-suite/{id:[a-zA-Z0-9-_]+}',
            'factory' => 'testSuite_One',
        ],
        TestSuiteHandlers\All::class => [
            'method' => 'GET',
            'route' => '/test-suite',
            'factory' => 'testSuite_All',
        ],
        RunHandlers\Create::class => [
            'method' => 'POST',
            'route' => '/run',
            'factory' => 'run_Create',
        ],
        RunHandlers\One::class => [
            'method' => 'GET',
            'route' => '/run/{id:[a-zA-Z0-9-_]+}',
            'factory' => 'run_One',
        ],
        RunHandlers\All::class => [
            'method' => 'GET',
            'route' => '/run',
            'factory' => 'run_All',
        ],
        ResultHandlers\One::class => [
            'method' => 'GET',
            'route' => '/run/{id:[a-zA-Z0-9-_]+}/results',
            'factory' => 'result_One',
        ],
    ];

    public static function create(ContainerInterface $container, ParameterBag $config): Router
    {
        if ($config->get('test-suites')['source']['type'] !== 'api') {
            unset(self::$routes[TestSuiteHandlers\Create::class]);
        }

        $routes = new RouteCollector(new Std(), new GroupCountBased());

        foreach (self::$routes as $service => $route) {
            $handler = function (ServerRequestInterface $request, ...$params) use ($service, $container, $route) {
                try {
                    $handler = $container->get($service);

                    if ($route['security'] === true) {
                        $handler = $container->get(Security::class)->wrap($handler);
                    }

                    return $handler($request, ...$params);
                } catch (\Throwable $exception) {
                    echo $exception->getMessage() . "\n";
                    throw $exception;
                }
            };
            $routes->addRoute($route['method'], $route['route'], $handler);
        }

        return new Router($routes);
    }
}
