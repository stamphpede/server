<?php

namespace Stamphpede\Server\Service;

use Psr\Log\LoggerInterface;
use React\Filesystem\FilesystemInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

class Filesystem
{
    private FilesystemInterface $filesystem;
    private ParameterBag $config;
    private LoggerInterface $logger;

    public function __construct(FilesystemInterface $reactFilesystem, ParameterBag $config, LoggerInterface $logger)
    {
        $this->filesystem = $reactFilesystem;
        $this->config = $config;
        $this->logger = $logger;
    }

    public function saveFile(string $filename, string &$contents, callable $then)
    {
        $filename = rtrim($this->config->get('workdir'), '/') . '/' . ltrim($filename, '/');

        $this->logger->info('Writing to file: ' . $filename);

        $this->filesystem->file($filename)->putContents($contents)->then(
            function ($message) use ($filename, $then) {
                $this->logger->info('File written');
                $then($filename);
            },
            function ($message) {
                $this->logger->error($message);
            }
        );
    }
}
