<?php

namespace Stamphpede\Server\Service;

use React\EventLoop\LoopInterface;
use React\Filesystem\Filesystem;
use React\Filesystem\FilesystemInterface;

class FilesystemFactory
{
    public static function create(LoopInterface $loop): FilesystemInterface
    {
        return Filesystem::create($loop);
    }
}
