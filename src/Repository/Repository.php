<?php

namespace Stamphpede\Server\Repository;

use Stamphpede\Server\Entity\EntityInterface;

interface Repository
{
    public function add(EntityInterface $element): void;

    public function remove(EntityInterface $element): void;

    public function get(string $key): ?EntityInterface;

    public function all(): array;
}
