<?php

namespace Stamphpede\Server\Repository;

use Stamphpede\Server\Entity\EntityInterface;

class InMemoryRepository implements Repository
{
    private array $data = [];

    public function add(EntityInterface $element): void
    {
        $this->data[$element->getId()] = $element;
    }

    public function remove(EntityInterface $element): void
    {
        unset($this->data[$element->getId()]);
    }

    public function get(string $key): ?EntityInterface
    {
        return $this->data[$key] ?? null;
    }

    public function all(): array
    {
        return $this->data;
    }
}
