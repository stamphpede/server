<?php

namespace Stamphpede\Server\Handler;

use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Sapient\CryptographyKeys\SigningPublicKey;
use ParagonIE\Sapient\Exception\HeaderMissingException;
use ParagonIE\Sapient\Exception\InvalidMessageException;
use ParagonIE\Sapient\Sapient;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Response;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

class Security implements Handler
{
    private Handler $wrapped;
    private SigningPublicKey $publicKey;

    public function __construct(ParameterBag $config)
    {
        $config = $config->get('test-suites');
        $this->publicKey = new SigningPublicKey(Base64UrlSafe::decode($config['source']['key']));
    }

    public function wrap(Handler $handler): Handler
    {
        $wrapper = clone $this;
        $wrapper->wrapped = $handler;
        return $wrapper;
    }

    public function __invoke(ServerRequestInterface $request, ...$routeParams): ResponseInterface
    {
        $sapient = new Sapient();
        try {
            $body = clone $request->getBody();
            $sapient->verifySignedRequest($request, $this->publicKey);
        } catch (HeaderMissingException|InvalidMessageException $exception) {
            return new Response(401);
        }

        $handler = $this->wrapped;

        return $handler($request->withBody($body), ...$routeParams);
    }
}
