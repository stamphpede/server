<?php

namespace Stamphpede\Server\Handler\Run;

use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Response;
use Stamphpede\Server\Runner\RunFactory;
use Stamphpede\Server\Runner\RunnerStats;
use Stamphpede\Server\Entity\Run;
use Stamphpede\Server\Entity\TestSuite;
use Stamphpede\Server\Generator\Generator;
use Stamphpede\Server\Handler\Handler;
use Stamphpede\Server\Repository\Repository;

class Create implements Handler
{
    private Repository $runRepository;
    private Generator $idGenerator;
    private SerializerInterface $serializer;
    private RunFactory $runFactory;
    private Repository $testSuiteRepository;

    public function __construct(
        Repository $runRepository,
        Repository $testSuiteRepository,
        Generator $idGenerator,
        SerializerInterface $serializer,
        RunFactory $runFactory
    ) {
        $this->runRepository = $runRepository;
        $this->idGenerator = $idGenerator;
        $this->serializer = $serializer;
        $this->runFactory = $runFactory;
        $this->testSuiteRepository = $testSuiteRepository;
    }

    public function __invoke(ServerRequestInterface $request, ...$routeParams): ResponseInterface
    {
        $data = json_decode($request->getBody(), \JSON_OBJECT_AS_ARRAY);
        $testSuite = $data['test_suite'];
        /** @var TestSuite $testSuiteEntity */
        $testSuiteEntity = $this->testSuiteRepository->get($testSuite);

        if ($testSuiteEntity === null) {
            return new Response(404);
        }

        $entity = new Run($this->idGenerator->generate(), $testSuite);

        $this->runFactory->createRun($testSuiteEntity)->then(
            function (RunnerStats $stats) use ($entity) {
                $entity->finished($stats);
            }
        );
        $this->runRepository->add($entity);
        return new Response(202, ['Content-Type' => 'application/hal+json'], $this->serializer->serialize($entity, 'json'));
    }
}
