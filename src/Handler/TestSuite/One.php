<?php

namespace Stamphpede\Server\Handler\TestSuite;

use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Response;
use Stamphpede\Server\Handler\Handler;
use Stamphpede\Server\Repository\Repository;

class One implements Handler
{
    private Repository $repository;
    private SerializerInterface $serializer;

    public function __construct(Repository $repository, SerializerInterface $serializer)
    {
        $this->repository = $repository;
        $this->serializer = $serializer;
    }

    public function __invoke(ServerRequestInterface $request, ...$routeParams): ResponseInterface
    {
        return new Response(200, ['Content-Type' => 'application/hal+json'], $this->serializer->serialize($this->repository->get($routeParams[0]), 'json'));
    }
}
