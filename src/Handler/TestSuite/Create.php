<?php

namespace Stamphpede\Server\Handler\TestSuite;

use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Response;
use Stamphpede\Parser\StamphpedeFactory;
use Stamphpede\Server\Entity\TestSuite;
use Stamphpede\Server\Generator\Generator;
use Stamphpede\Server\Handler\Handler;
use Stamphpede\Server\Repository\Repository;
use Stamphpede\Server\Service\Filesystem;

class Create implements Handler
{
    private Repository $repository;
    private Filesystem $filesystem;
    private Generator $idGenerator;
    private SerializerInterface $serializer;
    private StamphpedeFactory $stamphpedeFactory;

    public function __construct(
        Repository $repository,
        Filesystem $filesystem,
        Generator $idGenerator,
        SerializerInterface $serializer,
        StamphpedeFactory $stamphpedeFactory
    ) {
        $this->repository = $repository;
        $this->filesystem = $filesystem;
        $this->idGenerator = $idGenerator;
        $this->serializer = $serializer;
        $this->stamphpedeFactory = $stamphpedeFactory;
    }

    public function __invoke(ServerRequestInterface $request, ...$routeParams): ResponseInterface
    {
        $phpFile = (string) $request->getBody();
        $id = $this->generateId($phpFile);
        $entity = new TestSuite($id);
        $this->repository->add($entity);
        $fileName = '/testsuites/' . $id . '.php';

        $this->filesystem->saveFile($fileName, $phpFile, function ($fileName) use ($entity) {
            $entity->stamphpede($this->stamphpedeFactory->loadFile($fileName));
        });

        return new Response(202, ['Content-Type' => 'application/hal+json'], $this->serializer->serialize($entity, 'json'));
    }

    private function generateId(string &$phpFile): string
    {
        return rtrim(strtr(base64_encode(hash('sha384', $phpFile, true)), '+/', '-_'), '=');
    }
}
