<?php

namespace Stamphpede\Server\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface Handler
{
    public function __invoke(ServerRequestInterface $request, ...$routeParams): ResponseInterface;
}
