<?php

namespace Stamphpede\Server;

use Clue\React\Buzz\Browser as ReactBrowser;
use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use React\EventLoop\Factory as ReactEventLoopFactory;
use React\EventLoop\LoopInterface;
use React\Filesystem\FilesystemInterface;
use React\Http\Server;
use Stamphpede\Parser\StamphpedeFactory;
use Stamphpede\Server\Console\ServeCommand;
use Stamphpede\Server\Generator\Generator;
use Stamphpede\Server\Generator\YouTubeStyleIdentityGenerator;
use Stamphpede\Server\Repository\InMemoryRepository;
use Stamphpede\Server\Runner\Browser as StamphpedeBrowser;
use Stamphpede\Server\Runner\RunFactory;
use Stamphpede\Server\Service\Filesystem;
use Stamphpede\Server\Service\FilesystemFactory;
use Stamphpede\Server\Service\LogMultiplexer;
use Stamphpede\Server\Service\RouterFactory;
use Stamphpede\Server\Service\SerializerFactory;

class Module
{
    public function getServices(): array
    {
        return [
            LoopInterface::class => [
                'factory' => [ReactEventLoopFactory::class, 'create'],
            ],
            LoggerInterface::class => [
                'class' => LogMultiplexer::class,
            ],
            ReactBrowser::class => [
                'args' => [
                    LoopInterface::class,
                ],
            ],
            StamphpedeBrowser::class => [
                'args' => [
                    ReactBrowser::class,
                    LoggerInterface::class,
                ],
            ],
            RunFactory::class => [
                'args' => [
                    LoopInterface::class,
                    LoggerInterface::class,
                    StamphpedeBrowser::class,
                ],
            ],
            'testSuiteRepository' => [
                'class' => InMemoryRepository::class
            ],
            'runRepository' => [
                'class' => InMemoryRepository::class
            ],
            SerializerInterface::class => [
                'factory' => [SerializerFactory::class, 'create'],
            ],
            Generator::class => [
                'class' => YouTubeStyleIdentityGenerator::class,
            ],
            Filesystem::class => [
                'args' => [
                    FilesystemInterface::class,
                    'config',
                    LoggerInterface::class,
                ]
            ],
            FilesystemInterface::class => [
                'factory' => [FilesystemFactory::class, 'create'],
                'args' => [
                    LoopInterface::class,
                ]
            ],
            Router::class => [
                'factory' => [RouterFactory::class, 'create'],
                'args' => [
                    \Psr\Container\ContainerInterface::class,
                    'config',
                ]
            ],
            Server::class => [
                'args' => [
                    Router::class,
                ]
            ],
            ServeCommand::class => [
                'args' => [
                    Server::class,
                    LoggerInterface::class,
                    LoopInterface::class,
                ],
            ],

            Handler\TestSuite\All::class => [
                'args' => [
                    'testSuiteRepository',
                    SerializerInterface::class
                ]
            ],
            Handler\TestSuite\One::class => [
                'args' => [
                    'testSuiteRepository',
                    SerializerInterface::class
                ]
            ],
            Handler\TestSuite\Create::class => [
                'args' => [
                    'testSuiteRepository',
                    Filesystem::class,
                    Generator::class,
                    SerializerInterface::class,
                    StamphpedeFactory::class,
                ],
            ],

            Handler\Run\All::class => [
                'args' => [
                    'runRepository',
                    SerializerInterface::class
                ]
            ],
            Handler\Run\One::class => [
                'args' => [
                    'runRepository',
                    SerializerInterface::class
                ]
            ],
            Handler\Run\Create::class => [
                'args' => [
                    'runRepository',
                    'testSuiteRepository',
                    Generator::class,
                    SerializerInterface::class,
                    RunFactory::class,
                ],
            ],

            Handler\Result\One::class => [
                'args' => [
                    'runRepository',
                    SerializerInterface::class
                ]
            ],

            Handler\Security::class => [
                'args' => ['config'],
            ]
        ];
    }
}
